import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RegisterService } from '../services/register.service';
import { RegisterRequest, RegisterResponse } from '../../model/register.interface';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  formRegister!: FormGroup;
  isError: boolean = false;
  message?: string = '';

  constructor(private registerService: RegisterService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formRegister = this.fb.group({
      email: ['',
        [
          Validators.required,
          Validators.email,
        ],
      ],
      userName: ['',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(30),
        ],
      ],
      password: ['',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(20),
        ],
      ],
    });
  }


  callRegisterService() {
    if (this.formRegister.valid) {
      const body: RegisterRequest = {
        userName: this.formRegister.value.userName,
        email: this.formRegister.value.email,
        password: this.formRegister.value.password
      }
      this.registerService.postRegisterRequest(body).pipe().subscribe({
        next: (res) => {
          if (res.status === 204) {
            this.isError = true;
            this.message = 'User register. Please go to Login';
          }
          if (res.status === 200) {
            this.isError = true;
            this.message = res.body?.message;
          }
        },
        error: (e) => {
          this.isError = true;
          this.message = e.error.message
        }
      });
      this.formRegister.reset();
    }
  }
}
