import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from '../pages/home/home.component';
import { Error500Component } from './error/error500/error500.component';
import { Error404Component } from './error/error404/error404.component';
import { PagesRoutingModule } from './pages.routing';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon'
import { Error401Component } from './error/error401/error401.component';


@NgModule({
  declarations: [
    HomeComponent,
    Error500Component,
    Error404Component,
    Error401Component
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    PagesRoutingModule,
    MatIconModule
  ],
  exports: [HomeComponent]
})
export class PagesModule { }
