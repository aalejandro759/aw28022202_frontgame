import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ValidateRoutesGuard implements CanActivate {
  constructor(private router: Router) {
  }
  isAuth : boolean = false;
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    this.redirect(this.isAuth);
    return this.isAuth;
  }

  addAuth(){
    this.isAuth = true;
  }

  removeAuth(){
    this.isAuth = false;
  }
  redirect(flag:boolean): any {
    if(!flag){
      this.router.navigate(['/auth/login']);
    }
  }
}
