import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';
import { IpService } from '../../services/ip.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private ip: IpService) { }
  ipAddress: string = '';
  realTime: Date = new Date();
  ngOnInit(): void {
    timer(0,0).subscribe(() => {
      this.realTime = new Date();
    });
    this.getIp();  
  }

  getIp() {
    this.ip.getIPAddress().subscribe((res) => {
      this.ipAddress = res.ip;
    });
  }

}
