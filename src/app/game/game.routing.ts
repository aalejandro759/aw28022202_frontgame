import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../pages/home/home.component';
import { GameComponent } from './game.component';

export const routes: Routes = [ 
    {
        path: 'play/:coins',
        component: GameComponent
    }, 
    { 
        path: '**', 
        redirectTo: 'home'
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class GameRoutingModule { }
