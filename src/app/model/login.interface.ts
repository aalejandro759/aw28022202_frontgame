
export abstract class LoginResponse {
    abstract getBody(): any
}

export interface LoginRequest {
    email: string;
    password: string;
}



export interface LoginResponseSuccess {
    totalCredits: number;
    redirectTo: string;
    jwt: string;
}

export interface LoginResponseFaille{
    message: string;
    redirectTo: string;
}
