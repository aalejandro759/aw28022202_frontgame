import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { interval, take, map, concat, finalize } from 'rxjs';
import { fruitsGame } from '../shared/fruits.enum';
import { GameService } from './game.service';



@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {
  results: string[] = ['C', 'L', 'O'];
  credits: number = 0;
  isEnabled: boolean = false;
  isImage: boolean[] = [false, false, false]
  numberLeft: number = 0;
  numberTop: number = 0;
  isEnabledWallet : boolean = false;
  isEnabledCashOut : boolean = false;
  isError: boolean = false;
  message: string = 'do you want to Cash Out again?, play again';
  constructor(private serviceGame: GameService, private activatedRoute: ActivatedRoute) { }
  ngOnInit() {
    this.activatedRoute.params.subscribe(({ coins }) => {
      this.credits = coins;
    });
  }
  play(): void {
    this.isEnabled = true;
    this.isImage = [false, false, false]
    this.getGame();
    this.isEnabledCashOut = false;
  }

  getGame() {
    const observableService = this.serviceGame.postGameRequest().pipe(map(res => {
      this.results = res.resultGame;
      this.credits = res.totalCredits;
      return res;
    }));
    const intervalSpinner = interval(1000).pipe(take(3), map((v) => {
      this.isImage[v] = true;
    }), finalize(() => {
      this.isEnabled = false
    }));
    concat(observableService, intervalSpinner).subscribe();
  }
  
  playAgain(){
    this.isEnabledWallet = true;
  }

  moveButton(){
    const probability =  Math.floor(Math.random() * 100)
    if(probability <= 50){
      this.numberLeft = Math.floor(Math.random() * 300) - 150;
      this.numberTop = Math.floor(Math.random() * 300) - 150;
      return;
    }
    if(probability>50 && probability<90){
      this.isEnabledCashOut = true;
      this.isError = true;
      return;
    }
    
  }

}
