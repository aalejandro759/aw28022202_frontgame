export interface RegisterRequest{
        userName: string;
        email:    string;
        password: string;
}

export interface RegisterResponse{
    message:    string;
    redirectTo: string;
}