import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { RegisterRequest, RegisterResponse } from '../../model/register.interface';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {
  register: string = 'register';
  constructor(private httpClient: HttpClient) { }

  postRegisterRequest(body: RegisterRequest) {
    return this.httpClient.post<RegisterResponse>(
      `${environment.API}/${this.register}`, body, {observe: 'response'})
  }
}
