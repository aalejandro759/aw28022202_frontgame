import { TestBed } from '@angular/core/testing';

import { ValidateRoutesGuard } from './validate-routes.guard';

describe('ValidateRoutesGuard', () => {
  let guard: ValidateRoutesGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(ValidateRoutesGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
