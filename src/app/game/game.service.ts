import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GameResponse } from '../model/game.interface';

@Injectable({
  providedIn: 'root'
})
export class GameService {
  game: string = 'game';
  constructor(private httpClient: HttpClient) { }

  postGameRequest(){
    return this.httpClient.post<GameResponse>(
      `${environment.API}/${this.game}`,undefined)
  }
}
