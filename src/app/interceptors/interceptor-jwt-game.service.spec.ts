import { TestBed } from '@angular/core/testing';

import { InterceptorJwtGameService } from './interceptor-jwt-game.service';

describe('InterceptorJwtGameService', () => {
  let service: InterceptorJwtGameService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(InterceptorJwtGameService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
