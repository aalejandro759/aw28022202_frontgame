import { Component, OnInit } from '@angular/core';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginRequest } from 'src/app/model/login.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin!: FormGroup;
  isError: boolean = false;
  message: string = '';

  constructor(private loginService: LoginService, private fb: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.formLogin = this.fb.group({
      email: ['',
        [
          Validators.required,
          Validators.email,
        ],
      ],
      password: ['',
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(20),
        ],
      ],
    });
  }


 async callLoginService() {
    if (this.formLogin.valid) {
      const body: LoginRequest = {
        email: this.formLogin.value.email,
        password: this.formLogin.value.password
      }
     this.loginService.postLoginRequest(body).pipe().subscribe({
        next: (res) => {
          if (res.status === 204) {
            this.isError = true;
            this.message = 'User not found. Please create a new account';
          }
          if(res.status === 200){
             this.router.navigate(['/game/play',res.body?.totalCredits]);
          }
        },
        error: (e) => {
          this.isError = true;
          this.message = e.error.message
        }
      });
      this.formLogin.reset();
    }
  }

}
