export interface GameResponse {
    totalCredits: number;
    resultGame:   string[];
    jwt:          string;
}
