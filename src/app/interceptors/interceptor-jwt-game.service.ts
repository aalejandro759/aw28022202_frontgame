import { HttpEvent, HttpHandler, HttpHeaders, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, tap } from 'rxjs';
import { ValidateRoutesGuard } from '../helpers/validate-routes.guard';

@Injectable({
  providedIn: 'root'
})
export class InterceptorJwtGameService {

  transactionToken: string = '';
  constructor(private guard: ValidateRoutesGuard) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const transactionToken = this.transactionToken
    const requestWithHeaders = this.makeRequestWithHeaders(
      req,
      transactionToken
    );
    return next
      .handle(requestWithHeaders)
      .pipe(tap(res => this.extractTransactionToken(res as HttpResponse<any>)));
  }

  private makeRequestWithHeaders(
    request: HttpRequest<any>,
    transactionToken: string) {
    let headers: any;
    if (transactionToken) {
      headers = {
        'Authorization': 'Bearer ' + transactionToken
      };
    }
    headers = {
      ...headers,
    };
    return request.clone({
      setHeaders: headers
    });
  }

  private extractTransactionToken(response: HttpResponse<any>) {
    if (response && response.body) {
      this.transactionToken = response.body.jwt
      this.guard.addAuth();
    }
  }

}
