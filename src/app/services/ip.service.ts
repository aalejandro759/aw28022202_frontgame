import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IpResponse } from '../model/ip.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class IpService {
  constructor(private http: HttpClient) { }
  URLIp: string = 'http://api.ipify.org/?format=json'
  public getIPAddress(): Observable<IpResponse> {
    return this.http.get<IpResponse>(this.URLIp);
  }
}  