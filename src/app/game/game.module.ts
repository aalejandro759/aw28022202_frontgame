import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { InterceptorJwtGameService } from '../interceptors/interceptor-jwt-game.service';
import { GameRoutingModule } from './game.routing';

@NgModule({
  declarations: [
    GameComponent
  ],
  imports: [
    CommonModule,
    GameRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptorJwtGameService,
      multi: true
    }
  ]
})
export class GameModule { }
