import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginRequest, LoginResponseSuccess } from 'src/app/model/login.interface';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  login: string = 'login';
  constructor(private httpClient: HttpClient) { }

  postLoginRequest(body: LoginRequest) {
    return this.httpClient.post<LoginResponseSuccess>(
      `${environment.API}/${this.login}`, body, {observe: 'response'})
  }

}
